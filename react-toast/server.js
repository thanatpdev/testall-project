const express = require('express')
const path = require('path')
const app = express()
const cors = require('cors');


app.use(cors());

app.get('*.js', function (req, res, next) {
    console.log(req.url);
  if (req.url !== '/bundle.js') {
    req.url = req.url + '.gz'
    res.set('Content-Encoding', 'gzip')
  }
  next()
})
app.use(express.static(path.join(__dirname, 'public')))

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, 'public', 'index.html'))
})

app.listen(7830)
