module.exports = {
    apps: [
      {
        name: 'test-react',
        script: 'server.js',
        instances: 1,
        autorestart: true,
        watch: false,
        max_memory_restart: '1G',
        env: {
          mode: 'development',
        },
        env_production: {
          mode: 'production',
        },
      },
    ],
  };